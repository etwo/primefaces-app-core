<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.sprintin.primefaces</groupId>
  <artifactId>primefaces-core</artifactId>
  <version>1.0.9</version>
  <packaging>jar</packaging>

  <name>primefaces-core</name>
  <!--<url>git:releases://https://Wagon-Deployer-Sprint:Sp1nt1nn0v4ti0nS@bitbucket.org/etwo/primefaces-app-core.git</url>-->
  <url>git:snapshots://https://Wagon-Deployer-Sprint:Sp1nt1nn0v4ti0nS@bitbucket.org/etwo/primefaces-app-core.git</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <appengine.version>1.9.65</appengine.version>
  </properties>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.1</version>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <configuration>
          <additionalparam>-Xdoclint:none</additionalparam>
          <failOnError>false</failOnError>
        </configuration>
      </plugin>
    </plugins>

    <extensions>
      <extension>
        <groupId>ar.com.synergian</groupId>
        <artifactId>wagon-git</artifactId>
        <version>0.2.5</version>
      </extension>
    </extensions>
  </build>

  <distributionManagement>
    <repository>
      <id>sprintin-primefaces-core</id>
      <name>Sprint-in Primefaces Core</name>
      <url>git:releases://https://Wagon-Deployer-Sprint:Sp1nt1nn0v4ti0nS@bitbucket.org/etwo/primefaces-app-core.git</url>
    </repository>
    <snapshotRepository>
      <id>sprintin-primefaces-core</id>
      <name>Sprint-in Primefaces Core</name>
      <url>git:snapshots://https://Wagon-Deployer-Sprint:Sp1nt1nn0v4ti0nS@bitbucket.org/etwo/primefaces-app-core.git</url>
    </snapshotRepository>
  </distributionManagement>

  <pluginRepositories>
    <pluginRepository>
      <id>synergian-repo</id>
      <url>https://raw.github.com/synergian/wagon-git/snapshots</url>
      <!--<url>https://raw.github.com/synergian/wagon-git/releases</url>-->
    </pluginRepository>
  </pluginRepositories>

  <dependencies>
    <dependency>
      <groupId>com.google.appengine</groupId>
      <artifactId>appengine-api-1.0-sdk</artifactId>
      <version>${appengine.version}</version>
    </dependency>

    <!-- Hibernate-->
    <dependency>
      <groupId>org.hibernate</groupId>
      <artifactId>hibernate-entitymanager</artifactId>
      <version>4.2.0.Final</version>
    </dependency>

    <!-- Spring ORM -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-orm</artifactId>
      <version>4.3.1.RELEASE</version>
    </dependency>

    <!-- Spring Web -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-webmvc</artifactId>
      <version>4.3.1.RELEASE</version>
    </dependency>

    <!-- Spring Core -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-core</artifactId>
      <version>4.3.1.RELEASE</version>
    </dependency>

    <!-- Java EE Servlet -->
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
      <version>3.1.0</version>
    </dependency>

    <!-- MyFaces - JSF implementation -->
    <dependency>
      <groupId>org.apache.myfaces.core</groupId>
      <artifactId>myfaces-api</artifactId>
      <version>2.3.2</version>
    </dependency>

    <dependency>
      <groupId>org.apache.myfaces.core</groupId>
      <artifactId>myfaces-impl</artifactId>
      <version>2.3.2</version>
    </dependency>

    <!-- JSP 2.2 -->
    <dependency>
      <groupId>javax.servlet.jsp</groupId>
      <artifactId>jsp-api</artifactId>
      <version>2.2</version>
    </dependency>

    <!-- JSTL -->
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>jstl</artifactId>
      <version>1.2</version>
    </dependency>

    <!-- Java servlet 4.0 -->
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
      <version>4.0.1</version>
    </dependency>

    <!-- Websocket -->
    <dependency>
      <groupId>javax.websocket</groupId>
      <artifactId>javax.websocket-api</artifactId>
      <version>1.1</version>
    </dependency>

    <!-- CDI 2.0 -->
    <dependency>
      <groupId>javax.enterprise</groupId>
      <artifactId>cdi-api</artifactId>
      <version>2.0</version>
    </dependency>

    <!--SendGrid to send emails-->
    <dependency>
      <groupId>com.sendgrid</groupId>
      <artifactId>sendgrid-java</artifactId>
      <version>2.2.2</version>
    </dependency>

    <!-- Prime Faces -->
    <dependency>
      <groupId>org.primefaces</groupId>
      <artifactId>primefaces</artifactId>
      <version>6.2</version>
    </dependency>

    <!-- JSF EL API -->
    <dependency>
      <groupId>javax.el</groupId>
      <artifactId>javax.el-api</artifactId>
      <version>3.0.0</version>
    </dependency>

    <!-- JSF EL Implementation -->
    <dependency>
      <groupId>org.jboss.el</groupId>
      <artifactId>jboss-el</artifactId>
      <version>1.0_02.CR6</version>
    </dependency>

    <!--MD5 support-->
    <dependency>
      <groupId>commons-codec</groupId>
      <artifactId>commons-codec</artifactId>
      <version>1.9</version>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <repositories>
    <repository>
      <id>prime-repo</id>
      <name>Prime Repo</name>
      <url>http://repository.primefaces.org</url>
    </repository>

    <repository>
      <id>jboss</id>
      <name>Jboss Maven Repository</name>
      <url>https://repository.jboss.org/nexus/content/repositories/releases</url>
    </repository>
  </repositories>
</project>
